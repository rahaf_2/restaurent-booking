from datetime import datetime
from django.core.exceptions import ValidationError
from django.db import models
from account.models import User


def valid_time(value):
    if value < datetime.time(12, 0, 0) or value > datetime.time(23, 59, 0):
        raise ValidationError('Enter time between 12pm to 11:59pm')
    else:
        return value


def capacity_validation(value):
    if value > 12:
        raise ValidationError('Our Capacity is up to 12 person please Enter valid number')
    else:
        return value


def waiting_validation(value):
    avail_4_table = Tables.objects.filter(capacity=4).filter(is_avail=True).count()
    avail_6_table = Tables.objects.filter(capacity=6).filter(is_avail=True).count()
    if avail_4_table == 0 and avail_6_table:
        if value >= 4:
            raise ValidationError('Table for ' + str(value) + ' is available')
        else:
            return value

    elif avail_6_table == 0 and avail_4_table:
        if value <= 4:
            raise ValidationError('Table for ' + str(value) + ' is available')
        else:
            return value

    else:
        return value


class Tables(models.Model):
    user_id = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    table_number = models.CharField("Table Number", max_length=4, null=True)
    number_ofSeats = models.CharField("Number Of Seats", max_length=2, null=True)
    capacity = models.IntegerField("capacity", null=False)
    is_avail = models.BooleanField("Is avail", default=True)
    time = models.TimeField("time", null=True, blank=True)

    def __str__(self):
        return str(self.table_number)


# class Waiting(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     no_people = models.IntegerField(null=False, validators=[capacity_validation, waiting_validation])
#     add_time = models.TimeField(null=False, default=datetime.datetime.now().time())
#
#     def __str__(self):
#         return str(self.id) + " " + self.user.username
#
#
# class Booking(models.Model):
#     user = models.ForeignKey(User, on_delete=models.CASCADE)
#     table = models.ForeignKey(Tables, on_delete=models.CASCADE)
#     date = models.DateField()
#     time = models.TimeField(validators=[valid_time])
#     capacity = models.IntegerField()
#
#     def __str__(self):
#         return self.user.first_name + " " + str(self.table.table_number)
