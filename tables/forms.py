from datetime import datetime

from django import forms
from tables.models import Tables, Booking


class AddTables(forms.Form):

    table_number = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={'class': "form-control"}
        )
    )
    numberOfSeats = forms.IntegerField(
        widget=forms.NumberInput(
            attrs={'class': "form-control"}
        )
    )


# class BookingForm(forms.ModelForm):
#     capacity = forms.IntegerField(
#         widget=forms.NumberInput(
#             attrs={'class': 'form-control capacity', 'placeholder': 'Number of People'}))
#     date = forms.DateField(
#         widget=forms.DateInput(
#             attrs={'class': "form-control datepicker", 'type': 'date', "placeholder": "Date"}))
#     time = forms.TimeField(widget=forms.TimeInput(
#         attrs={'class': 'form-control timepicker', 'type': 'time'}))
#     table = forms.ModelChoiceField(
#         queryset=Tables.objects.none(),
#         empty_label='Select Table ID ',
#         widget=forms.Select(
#             attrs={'class': 'form-control', 'id': 'id_table', 'placeholder': 'Table Number'}
#         ))
#
#     class Meta:
#         model = Booking
#         fields = ['capacity', 'date', 'time', 'table']


def __init__(self, *args, **kwargs):
    super().__init__(*args, **kwargs)
    self.fields['table'].queryset = Tables.objects.none()
    if 'date' in self.data and 'time' in self.data:
        try:
            date = self.data.get('date')
            time = self.data.get('time')
            endtime = datetime.timedelta(hours=1)
            e_time = datetime.datetime.strptime(time, "%H:%M") - endtime
            Booked_table = Booking.objects.filter(date=date).filter(time__lte=time).filter(
                time__gte=e_time).values_list('table_id', flat=True)
            self.fields['table'].queryset = Tables.objects.exclude(id__in=Booked_table)
        except (ValueError, TypeError):
            pass
    elif self.instance.pk:
        self.fields['table'].queryset = Tables.objects.none()
