from django.urls import path
from . import views
from .views import logout_view

urlpatterns = [
    path('', views.index, name='index'),
    path('login', views.login_view, name = 'login_view'),
    path('logout/', logout_view, name='logout'),
    path('register/', views.register, name = 'register'),
    path('adminpage/', views.admin, name = 'adminpage'),
    path('employee/', views.employee, name='employee'),
    path('employee_management', views.employee_management, name='employee_management'),
    path('employee_editform/<int:pk>', views.editForm, name='employee_editform'),
    path('deleteEmployee/<int:pk>', views.deleteEmployee, name='deleteEmployee'),
]
