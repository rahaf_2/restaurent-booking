from django.shortcuts import render, redirect, get_object_or_404
from account.forms import SignUpForm, LoginForm
from django.contrib.auth import authenticate, login, logout, get_user_model
from account.models import User


def index(request):
    return render(request, 'index.html')


def register(request):
    msg = None
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            msg = 'user created'
            return redirect('employee_management')
        else:
            msg = 'form is not valid'

    else:
        form = SignUpForm()

    return render(request, 'register.html', {'form': form, 'msg': msg})


def login_view(request):
    form = LoginForm(request.POST or None)
    msg = None
    if request.method == 'POST':
        if form.is_valid():
            username = form.cleaned_data.get('username')
            password = form.cleaned_data.get('password')
            user = authenticate(username=username, password=password)
            if user is not None and user.is_admin:
                login(request, user)
                return redirect('adminpage')
            elif user is not None and user.is_employee:
                login(request, user)
                return redirect('employee')
            else:
                msg = 'invalid credentials'
        else:
            msg = 'error validating form'

    return render(request, 'login.html', {'form': form, 'msg': msg})


def admin(request):
    return render(request, 'admin.html')


def employee(request):
    return render(request, 'employee.html')


def logout_view(request):
    logout(request)
    return redirect('/')


def employee_management(request):
    objs = User.objects.all()
    return render(request, 'employee_management.html', {'objs': objs})


def editForm(request, pk):
    user = get_object_or_404(User, pk=pk)
    form = SignUpForm(request.GET or None, instance=user)
    if request.GET and form.is_valid():
        form.save()
        return redirect('employee_management')
    return render(request, 'employee_editform.html', {'form': form})


def deleteEmployee(request, pk, template_name='employee_management.html'):
    user = get_object_or_404(User, pk=pk)
    if request.method == 'GET':
        user.delete()
        return redirect('employee_management')
    return render(request, template_name, {'object': user})
