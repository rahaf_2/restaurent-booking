from django.db import models

from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    is_admin = models.BooleanField('Is admin', default=False)
    is_employee = models.BooleanField('Is Employee', default=False)

    @property
    def employee(self):
        return self.is_employee

    @property
    def admin(self):
        return self.is_admin

    @property
    def role(self):
        if self.is_admin:
            return 'admin'
        else:
            return 'employee'
