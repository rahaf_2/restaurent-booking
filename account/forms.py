from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError

from account.models import User


class LoginForm(forms.Form):
    username = forms.DecimalField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control"
            }
        ),
        max_digits=4
    )
    password = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control"
            }
        )
    )


class SignUpForm(UserCreationForm):
    CHOICES = [('admin', 'ADMEN'),
               ('employee', 'EMPLOYEE')]
    role = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect)
    username = forms.DecimalField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control"
            }
        ),
        max_digits=4
    )

    password1 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control"
            }
        )
    )
    password2 = forms.CharField(
        widget=forms.PasswordInput(
            attrs={
                "class": "form-control"
            }
        )
    )

    class Meta:
        model = User
        fields = ('username', 'password1', 'password2', 'is_admin', 'is_employee')



